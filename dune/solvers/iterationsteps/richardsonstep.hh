// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_RICHARDSON_STEP_HH
#define DUNE_RICHARDSON_STEP_HH

#include <dune/common/bitsetvector.hh>

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>

/** \brief A single preconditioned and damped Richardson step
*/
template<class VectorType,
         class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
         class RichardsonStep 
         : public IterationStep<VectorType, BitVectorType>
{

public:
        
    /** \brief Constructor */
    RichardsonStep(const Preconditioner<VectorType>* preconditioner,
                   double damping
    ) 
        : preconditioner_(preconditioner),
          damping_(damping)
    {}

    //! Perform one iteration
    virtual void iterate();

    const Preconditioner<VectorType>* preconditioner_;

    double damping_;
    
};

template<class VectorType, class BitVectorType>
inline
void RichardsonStep<VectorType, BitVectorType>::iterate()
{
    VectorType residual;
    
    preconditioner_->apply(*this->x_, residual);
    
    this->x_->axpy(damping_, residual);
    
}

#endif
