// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_NORMS_NORM_HH
#define DUNE_SOLVERS_NORMS_NORM_HH

#include <dune/common/ftraits.hh>

namespace Dune {
namespace Solvers {

//! Abstract base for classes computing norms of discrete functions
template <class V>
class Norm {

    public:
        typedef V VectorType;

        using field_type =  typename Dune::FieldTraits<VectorType>::field_type;

        /** \brief Destructor, doing nothing */
        virtual ~Norm() {}

        //! Compute the norm of the given vector
        virtual field_type operator()(const VectorType& f) const = 0;

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const
        {
            field_type r = this->operator()(f);
            return r*r;
        }

        //! Compute the norm of the difference of two vectors
        virtual field_type diff(const VectorType& f1, const VectorType& f2) const = 0;

};

} /* namespace Solvers */
} /* namespace Dune */

// For backward compatibility: will be removed eventually
using Dune::Solvers::Norm;

#endif
