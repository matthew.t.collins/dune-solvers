// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef MULTIGRID_TRANSFER_HH
#define MULTIGRID_TRANSFER_HH

#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>


/** \brief Restriction and prolongation operator for standard multigrid
 *
 * This class is the base class for prolongation and restriction
 * operators for standard multigrid solvers.  Restriction and prolongation
 * of block vectors is provided.

 * \todo Currently only works for first-order Lagrangian elements!
 */
template<
    class VectorType,
    class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension>,
    class MatrixType = Dune::BCRSMatrix< typename Dune::FieldMatrix<
        typename VectorType::field_type, VectorType::block_type::dimension, VectorType::block_type::dimension> > >
class MultigridTransfer {

public:

    typedef MatrixType OperatorType;

    virtual ~MultigridTransfer() {}


    /** \brief Restrict a function from the fine onto the coarse grid
     */
    virtual void restrict(const VectorType& f, VectorType &t) const = 0;


    /** \brief Restrict a bitfield from the fine onto the coarse grid
     */
    virtual void restrictScalarBitField(const Dune::BitSetVector<1>& f, Dune::BitSetVector<1>& t) const = 0;


    /** \brief Restrict a vector valued bitfield from the fine onto the coarse grid
     */
    virtual void restrict(const BitVectorType& f, BitVectorType& t) const = 0;


    /** \brief Restrict a vector valued bitfield from the fine onto the coarse grid
     * Fine bits only influence their father bits
     */
    virtual void restrictToFathers(const BitVectorType& f, BitVectorType& t) const = 0;


    /** \brief Prolong a function from the coarse onto the fine grid
     */
    virtual void prolong(const VectorType& f, VectorType &t) const = 0;


    /** \brief Galerkin assemble a coarse stiffness matrix
     */
    virtual void galerkinRestrict(const MatrixType& fineMat, MatrixType& coarseMat) const = 0;


    /** \brief Set Occupation of Galerkin restricted coarse stiffness matrix   
    *
    * Set occupation of Galerkin restricted coarse matrix. Call this one before
    * galerkinRestrict to ensure all non-zeroes are present
    * \param fineMat The fine level matrix
    * \param coarseMat The coarse level matrix
    */
    virtual void galerkinRestrictSetOccupation(const MatrixType& fineMat, MatrixType& coarseMat) const = 0;

};

#endif
